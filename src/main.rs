extern crate nom;
extern crate cse262_project;

use cse262_project::{program, run, Node};

fn main() -> Result<(), nom::Err<(&'static str, nom::error::ErrorKind)>> {
  let (unparsed, ast) = program("((10+2)*6)/4")?;
  println!("AST {:?}", ast);
  let result = run(&ast);
  println!("RESULT {:?}", result);
  Ok(())
}